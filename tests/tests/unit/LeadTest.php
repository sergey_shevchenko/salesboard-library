<?php


class LeadTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    /**
     * @var \Salesboard\Client\Client
     */
    protected $apiClient;

    protected function _before()
    {
        date_default_timezone_set('Europe/Amsterdam');
        $this->apiClient = new \Salesboard\Client\Client(\Salesboard\Client\Client::buildURL('beta'), 'admin', '1234');
    }

    protected function _after()
    {
    }

    // tests
    /**
     * @covers \Salesboard\Client\entities\Lead::getByID
     * @covers \Salesboard\Client\entities\Lead::getField
     * @throws \Salesboard\Client\exceptions\UnsuccessfulCallException
     */
    public function testGetByID()
    {
        $lead = \Salesboard\Client\entities\Lead::getByID($this->apiClient, 1);
        $this->assertEquals('2111AA', $lead->getField('postcode'));
        $this->assertEquals('1', $lead->getField('houseNumber'));
        $this->assertEquals('b', $lead->getField('houseSuffix'));
        $this->assertEquals('1', $lead->ID_Lead);
    }

    /**
     * @covers \Salesboard\Client\entities\Lead::update
     * @covers \Salesboard\Client\entities\Lead::fields
     *
     * @throws \Salesboard\Client\exceptions\LeadDuplicationException
     * @throws \Salesboard\Client\exceptions\UnsuccessfulCallException
     */
    public function testUpdate()
    {
        $lead = \Salesboard\Client\entities\Lead::getByID($this->apiClient, 1);
        $lead->fields()->houseSuffix = 'AUTOTEST';
        $originalUserAssignment = $lead->ID_User;
        $originalTeamAssignment = $lead->ID_Team;
        $lead->update();

        $lead = \Salesboard\Client\entities\Lead::getByID($this->apiClient, 1);

        $this->assertEquals('AUTOTEST', $lead->fields()->houseSuffix->value);
        $this->assertEquals($originalUserAssignment, $lead->ID_User);
        $this->assertEquals($originalTeamAssignment, $lead->ID_Team);
        $lead->fields()->houseSuffix = 'b';
        $lead->update();

        $lead = \Salesboard\Client\entities\Lead::getByID($this->apiClient, 1);

        $this->assertEquals('b', $lead->fields()->houseSuffix->value);
    }

    /**
     * @covers \Salesboard\Client\entities\Lead::dataLayers
     *
     * @throws \Salesboard\Client\exceptions\LeadDuplicationException
     * @throws \Salesboard\Client\exceptions\UnsuccessfulCallException
     */
    public function testDataLayers()
    {
        $lead = \Salesboard\Client\entities\Lead::getByID($this->apiClient, 1);
        $this->assertFalse($lead->dataLayers()->nosession->enabled);
        $lead->dataLayers()->nosession = true;
        $lead->update();

        $lead = \Salesboard\Client\entities\Lead::getByID($this->apiClient, 1);
        $this->assertTrue($lead->dataLayers()->nosession->enabled);
        $lead->dataLayers()->nosession = false;
        $lead->update();
    }

    /**
     * @covers \Salesboard\Client\entities\Lead::create
     *
     * @return int
     * @throws \Salesboard\Client\exceptions\LeadDuplicationException
     * @throws \Salesboard\Client\exceptions\UnsuccessfulCallException
     */
    public function testCreate()
    {
        $lead = \Salesboard\Client\entities\Lead::getByID($this->apiClient, 1);
        $lead->fields()->houseSuffix = 'AUTOTEST';
        $newID = $lead->create();

        $lead = \Salesboard\Client\entities\Lead::getByID($this->apiClient, $newID);

        $this->assertEquals('AUTOTEST', $lead->fields()->houseSuffix->value);

        return $newID;
    }

    /**
     * @depends testCreate
     * @covers  \Salesboard\Client\entities\Lead::assignToUser
     * @covers  \Salesboard\Client\entities\Lead::assignToTeam
     *
     * @param $id
     * @return
     */
    public function testAssignment($id)
    {
        $lead = $this->apiClient->getLead($id);
        $lead->assignToUser($this->apiClient->user);
        $lead->update();

        $lead = $this->apiClient->getLead($id);
        $this->assertEquals($this->apiClient->user->ID_User, $lead->ID_User, 'user assignment by object');
        $lead->assignToUser($this->apiClient->user->ID_User);
        $lead->update();

        $lead = $this->apiClient->getLead($id);
        $this->assertEquals($this->apiClient->user->ID_User, $lead->ID_User, 'user assignment by ID');
        $lead->assignToUser($this->apiClient->user->username);
        $lead->update();

        $lead = $this->apiClient->getLead($id);
        $this->assertEquals($this->apiClient->user->ID_User, $lead->ID_User, 'user assignment by username');
        $lead->assignToTeam($this->apiClient->getTeam(1));
        $lead->update();

        $lead = $this->apiClient->getLead($id);
        $this->assertEquals(1, $lead->ID_Team, 'lead assignment by object');
        $lead->assignToTeam(1);
        $lead->update();

        $lead = $this->apiClient->getLead($id);
        $this->assertEquals(1, $lead->ID_Team, 'lead assignment by ID');
        $lead->assignToTeam($this->apiClient->getTeam(1)->name);
        $lead->update();

        $lead = $this->apiClient->getLead($id);
        $this->assertEquals(1, $lead->ID_Team, 'lead assignment by name');


        return $id;
    }

    public function testListNoFields()
    {
        //Not Supported
        $this->markTestSkipped('Not supported yet');
    }

    public function testListFieldsTimeFiltered()
    {
        //TODO implement test
        $this->markTestSkipped('Test is not yet implemented');
    }


    /**
     * @depends testAssignment
     * @covers  \Salesboard\Client\entities\Lead::delete
     *
     * @param $id
     * @throws \Salesboard\Client\exceptions\UnsuccessfulCallException
     */
    public function testDelete($id)
    {
        $lead = \Salesboard\Client\entities\Lead::getByID($this->apiClient, $id);
        $this->assertEquals('AUTOTEST', $lead->fields()->houseSuffix->value);

        $lead->delete();

        $lead = \Salesboard\Client\entities\Lead::getByID($this->apiClient, $id);
        $this->assertNull($lead);
    }


}