<?php


class DocumentTest extends \Codeception\TestCase\Test
{
    /**
     * TODO: find a way to emulate and test deletion; as for now, it is tested manually
     */
    /**
     * @var \UnitTester
     */
    protected $tester;
    /**
     * @var \Salesboard\Client\Client
     */
    protected $apiClient;

    protected function _before()
    {
        date_default_timezone_set('Europe/Amsterdam');
        $this->apiClient = new \Salesboard\Client\Client(\Salesboard\Client\Client::buildURL('beta'), 'admin', '1234');
    }

    protected function _after()
    {
    }

    // tests
    /**
     * @covers \Salesboard\Client\entities\Document::getByID
     * @covers \Salesboard\Client\entities\Document::validateFields
     * @throws \Salesboard\Client\exceptions\UnsuccessfulCallException
     */
    public function testGetByID()
    {
        $document = \Salesboard\Client\entities\Document::getByID($this->apiClient, 2759);

        $this->assertInstanceOf('\Salesboard\Client\entities\Document', $document);
        $this->assertEquals(2759, $document->ID_Document);
        $this->assertTrue($document->validateFields());
    }

    /**
     * @covers \Salesboard\Client\entities\Document::hasPDF
     * @covers \Salesboard\Client\entities\Document::downloadFile
     * @covers \Salesboard\Client\entities\Document::validatePDF
     * @throws \Salesboard\Client\exceptions\UnsuccessfulCallException
     */
    public function testPDF()
    {
        $document = \Salesboard\Client\entities\Document::getByID($this->apiClient, 2759);
        $this->assertTrue($document->hasPDF());
        $filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . $document->PDF;
        $document->downloadFile($filename);
        $this->assertFileExists($filename);
        $this->assertTrue($document->validatePDF($filename));
        unlink($filename);
    }
}