<?php


class ClientTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    /**
     * @var \Salesboard\Client\Client
     */
    protected $apiClient;

    protected function _before()
    {
        date_default_timezone_set('Europe/Amsterdam');
        $this->apiClient = new \Salesboard\Client\Client(\Salesboard\Client\Client::buildURL('beta'), 'admin', '1234');
    }

    protected function _after()
    {
    }

    // tests
    /**
     * @covers \Salesboard\Client\Client::buildURL
     */
    public function testBuildURL()
    {
        $url = \Salesboard\Client\Client::buildURL('beta');

        $this->assertEquals('https://beta.salesboard.biz', $url);
    }

    /**
     * @covers \Salesboard\Client\Client::login
     */
    public function testLogin()
    {
        $this->apiClient->login();
        $this->apiClient->login();
        $this->assertInstanceOf('\Salesboard\Client\collections\Collection', $this->apiClient->getDocuments());
    }

    public function testAutoLogin()
    {
        $this->assertInstanceOf('\Salesboard\Client\collections\Collection', $this->apiClient->getDocuments());
    }

    /**
     * @covers \Salesboard\Client\Client::getDocuments
     * @covers \Salesboard\Client\Client::getLeads
     * @covers \Salesboard\Client\Client::getTeams
     * @covers \Salesboard\Client\Client::getUsers
     * @covers \Salesboard\Client\Client::getLeadDataLayers
     * @covers \Salesboard\Client\Client::getLeadFields
     */
    public function testCollections()
    {
        $this->assertInstanceOf('\Salesboard\Client\collections\Collection', $this->apiClient->getDocuments());
        $this->assertInstanceOf('\Salesboard\Client\collections\Collection', $this->apiClient->getLeads());
        $this->assertInstanceOf('\Salesboard\Client\entities\Team', current($this->apiClient->getTeams()));
        $this->assertInstanceOf('\Salesboard\Client\collections\Collection', $this->apiClient->getUsers());
        $this->assertInstanceOf('\Salesboard\Client\entities\LeadDataLayer', current($this->apiClient->getLeadDataLayers()));
        $this->assertInstanceOf('\Salesboard\Client\entities\LeadField', current($this->apiClient->getLeadFields()));
    }

    /**
     * @covers \Salesboard\Client\Client::getDocument
     * @covers \Salesboard\Client\Client::getLead
     * @covers \Salesboard\Client\Client::getUser
     * @covers \Salesboard\Client\Client::getTeam
     * @covers \Salesboard\Client\entities\Document::getByID
     * @covers \Salesboard\Client\entities\Lead::getByID
     * @covers \Salesboard\Client\entities\User::getByID
     * @covers \Salesboard\Client\entities\Team::getByID
     * @covers \Salesboard\Client\collections\TeamsCollection::getByID
     */
    public function testEntities()
    {
        $this->assertInstanceOf('\Salesboard\Client\entities\Entity', $this->apiClient->getDocument(1));
        $this->assertNull($this->apiClient->getDocument(1000000000));
        $this->assertInstanceOf('\Salesboard\Client\entities\Entity', $this->apiClient->getLead(1));
        $this->assertNull($this->apiClient->getLead(1000000000));
        $this->assertInstanceOf('\Salesboard\Client\entities\Entity', $this->apiClient->getUser($this->apiClient->user->ID_User));
        $this->assertNull($this->apiClient->getUser(1000000000));
        $this->assertInstanceOf('\Salesboard\Client\entities\Entity', $this->apiClient->getTeam(1));
        $this->assertNull($this->apiClient->getTeam(1000000000));
    }

    /**
     * @covers \Salesboard\Client\Client::beforeRequest
     * @covers \Salesboard\Client\Client::afterRequest
     */
    public function testRequestHook()
    {
        $this->apiClient->beforeRequest(function ($request) {
            $this->assertInstanceOf('\GuzzleHttp\Message\Request', $request);
            echo 'before';
        });
        $this->apiClient->afterRequest(function ($request, $response) {
            $this->assertInstanceOf('\GuzzleHttp\Message\Request', $request);
            $this->assertInstanceOf('\GuzzleHttp\Message\Response', $response);
            echo 'after';
        });

        ob_start();
        $this->apiClient->_get('http://example.com');
        $output = ob_get_contents();
        ob_end_clean();

        $this->assertEquals('beforeafter', $output);
    }

    /**
     * @covers \Salesboard\Client\Client::_get
     * @covers \Salesboard\Client\Client::_post
     * @covers \Salesboard\Client\Client::_put
     * @covers \Salesboard\Client\Client::_delete
     */
    public function testRequests()
    {
        $this->apiClient->beforeRequest(function ($request) {
            $this->assertEquals('GET', $request->getMethod());
        });
        $this->apiClient->_get('http://example.com');

        $this->apiClient->beforeRequest(function ($request) {
            $this->assertEquals('POST', $request->getMethod());
        });
        $this->apiClient->_post('http://example.com', 'some body');
        $this->apiClient->beforeRequest(function ($request) {
            $this->assertEquals('PUT', $request->getMethod());
        });
        $this->setExpectedException(
            'GuzzleHttp\Exception\ClientException',
            'Client error response [url] http://example.com [status code] 405 [reason phrase] Method Not Allowed'
        );
        $this->apiClient->_put('http://example.com', 'body');
        $this->apiClient->_delete('http://example.com');
    }

    /**
     * @covers \Salesboard\Client\Client::login
     */
    public function testLoginWrongCredentialException()
    {
        $this->apiClient = new \Salesboard\Client\Client(
            \Salesboard\Client\Client::buildURL('beta'),
            'admin',
            'wrong_password'
        );

        $this->setExpectedException(
            'Salesboard\Client\exceptions\AuthException',
            'Could not authenticate: username or password is incorrect'
        );
        $this->apiClient->login();
    }

    /**
     * @covers \Salesboard\Client\Client::login
     */
    public function testLoginUserExpiredException()
    {
        $this->apiClient = new \Salesboard\Client\Client(
            \Salesboard\Client\Client::buildURL('beta'),
            'user093209',
            '1234'
        );

        $this->setExpectedException(
            'Salesboard\Client\exceptions\AuthException',
            'Could not authenticate: user is expired'
        );
        $this->apiClient->login();
    }

}