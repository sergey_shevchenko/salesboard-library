<?php


class ActionTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    /**
     * @var \Salesboard\Client\Client
     */
    public $apiClient;

    protected function _before()
    {
        date_default_timezone_set('Europe/Amsterdam');
        $this->apiClient = new \Salesboard\Client\Client(\Salesboard\Client\Client::buildURL('beta'), 'admin', '1234');
    }

    protected function _after()
    {
    }

    // tests
    public function testLeadChanged()
    {
        $lead = $this->apiClient->getLead(1);
        $uniqueSuffixAddition = 'AUTOTEST' . substr(uniqid(), 0, 6);
        $lead->fields()->houseSuffix = $uniqueSuffixAddition;
        $new_id = $lead->create();

        $lead = $this->apiClient->getLead($new_id);


        $actionType = new \Salesboard\Client\actions\types\UpdateLeadActionType();
        $actionType->addLead($lead)
            ->addLeadDataLayer(current($lead->dataLayers()->all()));

        $action = new \Salesboard\Client\actions\Action($this->apiClient, [
            'duration'  => 0,
            'latitude'  => 0.123,
            'longitude' => 0.34,
            'siccess'   => true,
            'ID_Team'   => 1,
            'ID_User'   => $this->apiClient->user->ID_User,
            'time'      => time(),
            'type'      => $actionType
        ]);
        $response = $action->setConnection(\Salesboard\Client\actions\Action::CONNECTION_WIFI)
            ->setDuration(0)
            ->setLocation(0.123, 0.34)
            ->setSuccess(true)
            ->setTeam(1)
            ->setTeam($this->apiClient->getTeam(1))
            ->setUser($this->apiClient->user)
            ->setUser($this->apiClient->user->ID_User)
            ->setTime(time())
            ->setTime(date('Y-m-d H:i:s'))
            ->setType($actionType)
            ->push();

        $this->assertInstanceOf('\GuzzleHttp\Message\ResponseInterface', $response);

        $lead->delete();

        $this->setExpectedException('PHPUnit_Framework_Exception', "Unknown connection type provided assumed as 'NONE'");
        $action->setConnection('not_existing_connection_type');
    }

    public function testActionExceptions()
    {
        $this->setExpectedException('Exception', 'Invalid argument provided for action: type must be an instance of Salesboard\\Client\\actions\\types\\ActionType');
        $wrongAction = new \Salesboard\Client\actions\Action($this->apiClient, ['type' => 'not a type']);
    }

    public function testActionOnlineExceptions()
    {
        $this->setExpectedException('PHPUnit_Framework_Exception', "Unknown connection type provided assumed as 'NONE'");
        $wrongAction = new \Salesboard\Client\actions\Action($this->apiClient, ['online' => 'not a valid online type']);
    }
}