<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 15/12/15
 * Time: 11:34
 */

namespace Salesboard\Client;


use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Message\Request;
use GuzzleHttp\Post\PostBody;
use GuzzleHttp\Url;
use Salesboard\Client\collections\DocumentsCollection;
use Salesboard\Client\collections\LeadDataLayersStaticCollection;
use Salesboard\Client\collections\LeadFieldsStaticCollection;
use Salesboard\Client\collections\LeadsCollection;
use Salesboard\Client\collections\TeamsCollection;
use Salesboard\Client\collections\UsersCollection;
use Salesboard\Client\entities\Document;
use Salesboard\Client\entities\Lead;
use Salesboard\Client\entities\Team;
use Salesboard\Client\entities\User;
use Salesboard\Client\exceptions\APIException;
use Salesboard\Client\exceptions\AuthException;

/**
 * Class Client
 * @package Salesboard\Client
 */
class Client
{
    public $url;
    /**
     * @var callable
     */
    public $beforeRequest = null;
    /**
     * @var callable
     */
    public $afterRequest = null;
    /**
     * @var User
     */
    public $user;
    public $deviceID = 'SalesboardAPILibrary';

    /**
     * @var \GuzzleHttp\Client $client
     */
    private $client;
    private $username;
    private $password;
    private $jar;
    private $defaultHeaders = [
        'XREQUESTEDWITH' => 'APIHttpRequest',
        'appVersion'     => '0.0.1'
    ];
    private $loggedIn = false;

    /**
     * Client constructor.
     * @param $url
     * @param $username
     * @param $password
     */
    public function __construct($url, $username, $password)
    {
        $this->url = $url;
        $this->username = $username;
        $this->password = $password;

        $this->client = new \GuzzleHttp\Client([
            'base_url' => [$url, []],
            'defaults' => [
                'timeout'         => 30,
                'allow_redirects' => false,
            ]
        ]);
        $this->jar = new CookieJar();
    }

    /**
     * @return $this
     * @throws APIException
     * @throws AuthException
     */
    public function login()
    {
        if ($this->loggedIn) {
            //prevent unnecessary calls
            return $this;
        }

        $response = $this->_post(
            '/login',
            '',
            [
                'username' => $this->username,
                'password' => $this->password
            ]
        );

        $responseBody = json_decode($response->getBody());
        if (!$responseBody->success) {

            switch ($responseBody->errors[0]) {
                case 'ERROR_AUTHENTICATION_FAILED':
                    throw new AuthException(100, $response);
                    break;
                case 'ERROR_NO_ACCESS':
                    throw new AuthException(101, $response);
                    break;
                case 'ERROR_USER_EXPIRED':
                    throw new AuthException(102, $response);
                    break;
                default:
                    throw new AuthException(103, $response);
            }
        }

        $this->jar->fromArray(['PHPSESSID' => $responseBody->responseData], $this->url);
        $this->loggedIn = true;

        $response = $this->_get('/app-info');

        $responseBody = json_decode($response->getBody());

        $this->user = new User($this, json_decode(json_encode($responseBody->responseData->currentUser), true));

        return $this;
    }

    /**
     * @param $client
     * @return string
     */
    static function buildURL($client)
    {
        return 'https://' . $client . '.salesboard.biz';
    }

    /**
     * @param callable $callable
     */
    public function beforeRequest(callable $callable)
    {
        $this->beforeRequest = $callable;
    }

    /**
     * @param callable $callable
     */
    public function afterRequest(callable $callable)
    {
        $this->afterRequest = $callable;
    }

    /**
     * @param string $url
     * @param string $body
     * @param array  $formParams
     * @param array  $options
     * @return \GuzzleHttp\Message\ResponseInterface
     * @throws APIException
     */
    public function _post($url, $body = '', $formParams = [], $options = [])
    {
        if (!empty($body)) {
            $options['body'] = $body;
        }
        if (!empty($formParams)) {
            $options['body'] = $formParams;
        }

        return $this->_request($url, 'POST', $options);
    }

    /**
     * @param string $url
     * @param array  $getParams
     * @param array  $options
     * @return \GuzzleHttp\Message\ResponseInterface
     * @throws APIException
     */
    public function _get($url, $getParams = [], $options = [])
    {
        if (!empty($getParams)) {
            $options['query'] = $getParams;
        }

        return $this->_request($url, 'GET', $options);
    }

    /**
     * @param string $url
     * @param string $body
     * @param array  $options
     * @return \GuzzleHttp\Message\ResponseInterface
     * @throws APIException
     */
    public function _put($url, $body, $options = [])
    {
        if (!empty($body)) {
            $options['body'] = $body;
        }

        return $this->_request($url, 'PUT', $options);
    }

    /**
     * @param string $url
     * @param array  $options
     * @return \GuzzleHttp\Message\ResponseInterface
     * @throws APIException
     */
    public function _delete($url, $options = [])
    {
        return $this->_request($url, 'DELETE', $options);
    }

    /**
     * @param int $page
     * @return DocumentsCollection
     */
    public function getDocuments($page = 1)
    {
        return new DocumentsCollection($this, $page);
    }

    /**
     * @param int $id
     * @return Document
     * @throws exceptions\UnsuccessfulCallException
     */
    public function getDocument($id)
    {
        return Document::getByID($this, $id);
    }

    /**
     * @param int $page
     * @return UsersCollection
     */
    public function getUsers($page = 1)
    {
        return new UsersCollection($this, $page);
    }

    /**
     * @param $id
     * @return null|User
     * @throws exceptions\UnsuccessfulCallException
     */
    public function getUser($id)
    {
        return User::getByID($this, $id);
    }

    /**
     * @return Team[]
     */
    public function getTeams()
    {
        return TeamsCollection::getInstance($this)->all();
    }

    /**
     * @param $id
     * @return null|Team
     */
    public function getTeam($id)
    {
        return Team::getByID($this, $id);
    }

    /**
     * @param int $page
     * @return LeadsCollection
     */
    public function getLeads($page = 1)
    {
        return new LeadsCollection($this, $page);
    }

    /**
     * @param int $id
     * @return null|Lead
     * @throws exceptions\UnsuccessfulCallException
     */
    public function getLead($id)
    {
        return Lead::getByID($this, $id);
    }

    /**
     * @return \Salesboard\Client\entities\LeadDataLayer[]
     */
    public function getLeadDataLayers()
    {
        return LeadDataLayersStaticCollection::getInstance($this)->all();
    }

    /**
     * @return \Salesboard\Client\entities\LeadField[]
     */
    public function getLeadFields()
    {
        return LeadFieldsStaticCollection::getInstance($this)->all();
    }

    /**
     * @param      $url
     * @param      $method
     * @param      $options
     * @param bool $retryOn403
     * @return \GuzzleHttp\Message\FutureResponse|\GuzzleHttp\Message\ResponseInterface|\GuzzleHttp\Ring\Future\FutureInterface|null
     * @throws APIException
     * @throws AuthException
     */
    private function _request($url, $method, $options, $retryOn403 = true)
    {
        if (array_key_exists('headers', $options)) {
            $options['headers'] = array_merge(
                $options['headers'],
                $this->defaultHeaders
            );
        } else {
            $options['headers'] = $this->defaultHeaders;
        }
        $options['cookies'] = $this->jar;

        $request = $this->client->createRequest($method, $url, $options);
        if (!is_null($this->beforeRequest)) {
            call_user_func_array($this->beforeRequest, [$request]);
        }

        try {
            $response = $this->client->send($request);
        } catch (ClientException $e) {
            if (404 === $e->getResponse()->getStatusCode() && $retryOn403) {
                //backwards compatibility... again
                $this->loggedIn = false;
                $this->login();
                $response = $this->_request($url, $method, $options, false);
            } else {
                throw $e;
            }

        }

        if ($retryOn403 && $response->getStatusCode() === 200) {
            $responseBody = json_decode($response->getBody()->getContents());

            if (is_object($responseBody) && !$responseBody->success) { //some responses may contain no-JSON data, such as files
                $error = array_pop($responseBody->errors);
                if (
                    (is_object($error) && 403 === $error->status)
                    || (is_string($error) && 'ERROR_NO_ACCESS' === $error) //backwards compatibility
                ) {
                    //session expired, try to re-login
                    $this->loggedIn = false;
                    $this->login();
                    $response = $this->_request($url, $method, $options, false);
                }
            }
        } elseif ($retryOn403 && 404 === $response->getStatusCode()) {  //more backwards compatibility
            $this->loggedIn = false;
            $this->login();
            $response = $this->_request($url, $method, $options, false);
        }

        if (!is_null($this->afterRequest)) {
            call_user_func_array($this->afterRequest, [$request, $response]);
        }

        if ($response->getStatusCode() < 200 || $response->getStatusCode() > 299) {
            throw new APIException($response->getStatusCode(), $method, $url, 001, $response);
        }

        return $response;
    }
}