<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 21/12/15
 * Time: 10:47
 */

namespace Salesboard\Client\nameSpaces;


use Salesboard\Client\Client;
use Salesboard\Client\collections\LeadDataLayersStaticCollection;
use Salesboard\Client\collections\StaticCollection;
use Salesboard\Client\entities\Entity;
use Salesboard\Client\entities\Lead;
use Salesboard\Client\entities\LeadDataLayer;

class DataLayersNameSpace
{

    /**
     * @var Lead
     */
    protected $_parent;
    /**
     * @var StaticCollection
     */
    protected $_collection;

    /**
     * @var LeadDataLayer[]
     */
    protected $_available;
    /**
     * @var LeadDataLayer[]
     */
    protected $_set;
    private $_ids;

    /**
     * DataLayersNameSpace constructor.
     * @param Client $client
     * @param Entity $parent
     */
    public function __construct(Client $client, Entity $parent)
    {
        $this->_parent = $parent;
        $this->_collection = LeadDataLayersStaticCollection::getInstance($client);

        foreach ($this->_collection->all() as $key => $value) {
            $this->_available[$key] = $value;
            $this->_ids[$value->ID_Data_layer] = $value->name;
        }

        foreach ($this->_parent->dataLayers as $key => $value) {
            $this->_set[$key] = clone $this->_available[$key];
            $this->_set[$key]->enabled = $value->enabled;
        }
    }

    /**
     * @param string $name
     * @return null|LeadDataLayer
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->_set)) {
            return $this->_set[$name];
        }

        if (array_key_exists($name, $this->_available)) {
            $clone = clone $this->_available[$name];
            $clone->enabled = false;
            return $clone;
        }

        return null;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        if ($value) {
            $this->_enableByName($name);
        } else {
            $this->_disableByName($name);
        }
    }

    /**
     * @param string|int|LeadDataLayer $dataLayer
     */
    public function assign($dataLayer)
    {
        if ($dataLayer instanceof LeadDataLayer) {
            $this->_enableByName($dataLayer->name);
        } elseif (is_numeric($dataLayer)) {
            //assume this is an ID
            if (array_key_exists($dataLayer, $this->_ids)) {
                $this->_enableByName($this->_ids[$dataLayer]);
            }
        } elseif (is_string($dataLayer)) {
            //assume this is a DL name
            $this->_enableByName($dataLayer);
        }
    }

    /**
     * @param string|int|LeadDataLayer $dataLayer
     */
    public function deassign($dataLayer)
    {
        if ($dataLayer instanceof LeadDataLayer) {
            $this->_disableByName($dataLayer->name);
        } elseif (is_numeric($dataLayer)) {
            //assume this is an ID
            if (array_key_exists($dataLayer, $this->_ids)) {
                $this->_disableByName($this->_ids[$dataLayer]);
            }
        } elseif (is_string($dataLayer)) {
            //assume this is a DL name
            $this->_disableByName($dataLayer);
        }
    }

    /**
     *
     */
    public function refresh()
    {
        $this->_collection->refresh();
        foreach ($this->_collection as $key => $value) {
            $this->_available[$key] = $value;
            $this->_ids[$value->ID_Data_layer] = $value->name;
        }
    }

    /**
     * @param string $name
     */
    private function _enableByName($name)
    {
        if (array_key_exists($name, $this->_set)) {
            $this->_set[$name]->enabled = true;
        } elseif (array_key_exists($name, $this->_available)) {
            $this->_set[$name] = clone $this->_available[$name];
            $this->_set[$name]->enabled = true;
        }
    }

    /**
     * @param string $name
     */
    private function _disableByName($name)
    {
        if (array_key_exists($name, $this->_set)) {
            $this->_set[$name]->enabled = false;
        }
    }

    /**
     * @return \Salesboard\Client\entities\LeadDataLayer[]
     */
    public function all()
    {
        return $this->_set;
    }

}