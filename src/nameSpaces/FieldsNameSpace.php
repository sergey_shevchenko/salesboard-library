<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 21/12/15
 * Time: 11:01
 */

namespace Salesboard\Client\nameSpaces;


use Salesboard\Client\Client;
use Salesboard\Client\collections\LeadFieldsStaticCollection;
use Salesboard\Client\collections\StaticCollection;
use Salesboard\Client\entities\Entity;
use Salesboard\Client\entities\Lead;
use Salesboard\Client\entities\LeadField;

class FieldsNameSpace
{
    /**
     * @var Lead
     */
    protected $_parent;
    /**
     * @var StaticCollection
     */
    protected $_collection;

    /**
     * @var LeadField[]
     */
    protected $_available;
    /**
     * @var LeadField[]
     */
    protected $_set;

    /**
     * FieldsNameSpace constructor.
     * @param Client $client
     * @param Entity $parent
     */
    public function __construct(Client $client, Entity $parent)
    {
        $this->_parent = $parent;
        $this->_collection = LeadFieldsStaticCollection::getInstance($client);

        foreach ($this->_collection->all() as $key => $value) {
            $this->_available[$key] = $value;
        }

        foreach ($this->_parent->fields as $key => $value) {
            $this->_set[$key] = clone $this->_available[$key];
            $this->_set[$key]->value = $value->value;
        }
    }

    /**
     * @param string $name
     * @return null|LeadField
     */
    public function __get($name)
    {

        if (array_key_exists($name, $this->_set)) {
            return $this->_set[$name];
        }

        if (array_key_exists($name, $this->_available)) {
            $clone = clone $this->_available[$name];
            $clone->value = null;

            return $clone;
        }

        return null;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->_set)) {
            $this->_set[$name]->value = $value;
        } elseif (array_key_exists($name, $this->_available)) {
            $this->_set[$name] = clone $this->_available[$name];
            $this->_set[$name]->value = $value;
        }
    }

    /**
     *
     */
    public function refresh()
    {
        $this->_collection->refresh();
        foreach ($this->_collection as $key => $value) {
            $this->_available[$key] = $value;
        }
    }

    /**
     * @return \Salesboard\Client\entities\LeadField[]
     */
    public function all()
    {
        return $this->_set;
    }
}