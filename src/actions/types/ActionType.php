<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 21/12/15
 * Time: 16:40
 */

namespace Salesboard\Client\actions\types;


class ActionType
{
    const CATEGORY_MODEL = 'model';
    const CATEGORY_SALESBOARD = 'salesboard';
    const CATEGORY_SYSTEM = 'system';

    public $category;
    public $type;
    public $details;

    /**
     * ActionType constructor.
     * @param       $category
     * @param       $type
     * @param array $details
     * @throws \Exception
     */
    public function __construct($category, $type, array $details)
    {
        $oClass = new \ReflectionClass(__CLASS__);
        if (!in_array($category, $oClass->getConstants())) {
            throw new \Exception("Unknown category provided for action type $type");
        }

        $this->category = $category;
        $this->type = $type;
        $this->details = empty($details) ? new \stdClass() : $details;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->type;
    }
}