<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 21/12/15
 * Time: 17:28
 */

namespace Salesboard\Client\actions\types;


use GuzzleHttp\Message\RequestInterface;
use Salesboard\Client\entities\Lead;
use Salesboard\Client\entities\LeadDataLayer;
use Salesboard\Client\entities\LeadField;

class UpdateLeadActionType extends ActionType
{
    /**
     * UpdateLeadActionType constructor.
     */
    public function __construct()
    {
        $category = ActionType::CATEGORY_SALESBOARD;
        $type = 'leadChanged';
        $details = [
            'ID_Lead'      => '',
            'ID_DataLayer' => '',
            'ID_LeadField' => '',
            'dlEnabled'    => ''
        ];

        $this->complete = false;

        parent::__construct($category, $type, $details);
    }

    /**
     * @param Lead $lead
     * @return $this
     */
    public function addLead(Lead $lead)
    {
        $this->details['ID_Lead'] = $lead->ID_Lead;
        return $this;
    }

    /**
     * @param $id
     * @return $this
     */
    public function addLeadByID($id)
    {
        $this->details['ID_Lead'] = $id;
        return $this;
    }

    /**
     * @param LeadField $field
     * @return $this
     */
    public function addLeadField(LeadField $field)
    {
        $this->details['ID_LeadField'] = $field->ID_Field;
        return $this;
    }

    /**
     * @param LeadDataLayer $dataLayer
     * @return $this
     */
    public function addLeadDataLayer(LeadDataLayer $dataLayer)
    {
        $this->details['ID_DataLayer'] = $dataLayer->ID_Data_layer;
        $this->details['dlEnabled'] = $dataLayer->enabled;
        return $this;
    }

    /**
     * @param RequestInterface $requestInterface
     * @return UpdateLeadActionType
     */
    public static function createFromRequest(requestInterface $requestInterface)
    {
        $type = new UpdateLeadActionType();

        $type->addLeadByID(
            $requestInterface->getQuery()->get('ID_Lead')
        );
        if(!empty($requestInterface->getQuery()->get('leadFields'))) {

        }

        return $type;
    }

}