<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 21/12/15
 * Time: 17:28
 */

namespace Salesboard\Client\actions\types;


use GuzzleHttp\Message\RequestInterface;
use Salesboard\Client\entities\Document;

class DeleteDocumentActionType extends ActionType
{
    /**
     * DeleteDocumentActionType constructor.
     */
    public function __construct()
    {
        $category = ActionType::CATEGORY_SALESBOARD;
        $type = 'documentDeleted';
        $details = [
            'ID_Document' => '',
        ];

        $this->complete = false;

        parent::__construct($category, $type, $details);
    }

    /**
     * @param Document $document
     * @return $this
     */
    public function addDocument(Document $document)
    {
        $this->details['ID_Document'] = $document->ID_Document;
        return $this;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function addDocumentID($id)
    {
        $this->details['ID_Document'] = $id;
        return $this;
    }

    /**
     * @param RequestInterface $requestInterface
     * @return DeleteDocumentActionType
     */
    public static function createFromRequest(requestInterface $requestInterface)
    {
        $type = new DeleteDocumentActionType();

        $id = explode('/', $requestInterface->getPath());
        $type->addDocumentID(
            array_pop($id)
        );

        return $type;

    }
}