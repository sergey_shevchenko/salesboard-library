<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 21/12/15
 * Time: 17:28
 */

namespace Salesboard\Client\actions\types;


use GuzzleHttp\Message\RequestInterface;
use Salesboard\Client\entities\Lead;

class CreateLeadActionType extends ActionType
{
    /**
     * CreateLeadActionType constructor.
     */
    public function __construct()
    {
        $category = ActionType::CATEGORY_SALESBOARD;
        $type = 'leadCreated';
        $details = [
            'ID_Lead'      => '',
        ];

        $this->complete = false;

        parent::__construct($category, $type, $details);
    }

    /**
     * @param Lead $lead
     * @return $this
     */
    public function addLead(Lead $lead)
    {
        $this->details['ID_Lead'] = $lead->ID_Lead;
        return $this;
    }

    /**
     * @param $id
     * @return $this
     */
    public function addLeadByID($id)
    {
        $this->details['ID_Lead'] = $id;
        return $this;
    }

    /**
     * @param RequestInterface $requestInterface
     * @return CreateLeadActionType
     */
    public static function createFromRequest(requestInterface $requestInterface)
    {
        $type = new CreateLeadActionType();

        $type->addLeadByID(
            json_decode($requestInterface->getBody()->getContents())->ID_Lead
        );

        return $type;

    }
}