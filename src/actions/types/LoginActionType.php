<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 21/12/15
 * Time: 17:28
 */

namespace Salesboard\Client\actions\types;



use GuzzleHttp\Message\RequestInterface;

class LoginActionType extends ActionType
{
    /**
     * LoginActionType constructor.
     */
    public function __construct()
    {
        $category = ActionType::CATEGORY_SALESBOARD;
        $type = 'login';
        $details = [];

        parent::__construct($category, $type, $details);
    }

    public static function createFromRequest(requestInterface $requestInterface)
    {
        return new LoginActionType();
    }
}