<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 21/12/15
 * Time: 17:28
 */

namespace Salesboard\Client\actions\types;


use GuzzleHttp\Message\RequestInterface;

class LogoutActionType extends ActionType
{
    /**
     * LogoutActionType constructor.
     */
    public function __construct()
    {
        $category = ActionType::CATEGORY_SALESBOARD;
        $type = 'logout';
        $details = [];

        parent::__construct($category, $type, $details);
    }

    /**
     * @param RequestInterface $requestInterface
     * @return LogoutActionType
     */
    public static function createFromRequest(requestInterface $requestInterface)
    {
        return new LogoutActionType();
    }
}