<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 21/12/15
 * Time: 15:55
 */

namespace Salesboard\Client\actions;


use GuzzleHttp\Message\RequestInterface;
use Salesboard\Client\actions\types\ActionType;
use Salesboard\Client\Client;
use Salesboard\Client\entities\Team;
use Salesboard\Client\entities\User;
use Salesboard\Client\exceptions\UnsuccessfulCallException;

class Action
{
    const CONNECTION_NONE = 'NONE';
    const CONNECTION_2G = '2G';
    const CONNECTION_EDGE = '2G';
    const CONNECTION_GPRS = '2G';
    const CONNECTION_3G = '3G';
    const CONNECTION_WCDMA = '3G';
    const CONNECTION_HSDPA = '3G';
    const CONNECTION_HSUPA = '3G';
    const CONNECTION_CDMA1x = '2G';
    const CONNECTION_CDMAEVD0Rev0 = '3G';
    const CONNECTION_CDMAEVD0RevA = '3G';
    const CONNECTION_CDMAEVD0RevB = '3G';
    const CONNECTION_eHRPD = '3G';
    const CONNECTION_4G = '4G';
    const CONNECTION_LTE = '4G';
    const CONNECTION_WIFI = 'wifi';

    public $ID_User;
    public $ID_Team;
    public $latitude = 0;
    public $longitude = 0;
    public $connection = self::CONNECTION_NONE;
    public $duration = 0;
    public $success = true;
    public $createdAt;
    /**
     * @var ActionType
     */
    public $type;

    private $_client;

    private static $_urlClassRelations = [
        'login'     => '\Salesboard\Client\actions\types\LoginActionType',
        'logout'    => '\Salesboard\Client\actions\types\LogoutActionType',
        'documents' => [
            'DELETE' => '\Salesboard\Client\actions\types\DeleteDocumentActionType',
        ],
        'leads'     => [
            'POST'   => '\Salesboard\Client\actions\types\CreateLeadActionType',
            'PUT'    => '\Salesboard\Client\actions\types\UpdateLeadActionType',
            'DELETE' => '\Salesboard\Client\actions\types\DeleteLeadActionType',
        ]
    ];

    /**
     * Action constructor.
     * @param Client $client
     * @param array  $params
     * @throws \Exception
     */
    public function __construct(Client $client, array $params = null)
    {
        $error = false;
        if (!is_null($params)) {
            if (array_key_exists('type', $params)) {
                if (!($params['type'] instanceof ActionType)) {
                    throw new \Exception("Invalid argument provided for action: type must be an instance of Salesboard\\Client\\actions\\types\\ActionType");
                }
            }

            if (array_key_exists('online', $params)) {
                //make it consistent with APIs
                $params['connection'] = $params['online'];
            }

            if (array_key_exists('connection', $params)) {
                $oClass = new \ReflectionClass(__CLASS__);
                if (!in_array($params['connection'], $oClass->getConstants())) {
                    $params['connection'] = self::CONNECTION_NONE;
                    $error = true;
                }
            }
        }

        $this->_client = $client;

        foreach ($params as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }

        if ($error) trigger_error("Unknown connection type provided assumed as 'NONE'", E_USER_NOTICE);
    }

    /**
     * @param User|int $user
     * @return $this
     */
    public function setUser($user)
    {
        if ($user instanceof User) {
            $this->ID_User = $user->ID_User;
        } else {
            $this->ID_User = $user;
        }

        return $this;
    }

    /**
     * @param Team|int $team
     * @return $this
     */
    public function setTeam($team)
    {
        if ($team instanceof Team) {
            $this->ID_team = $team->ID_Team;
        } else {
            $this->ID_Team = $team;
        }

        return $this;
    }

    /**
     * @param float $latitude
     * @param float $longitude
     * @return $this
     */
    public function setLocation($latitude, $longitude)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * @param string $connection
     * @return $this
     */
    public function setConnection($connection)
    {
        $error = false;
        $oClass = new \ReflectionClass(__CLASS__);
        if (!in_array($connection, $oClass->getConstants())) {
            $connection = self::CONNECTION_NONE;
            $error = true;
        }

        if ($error) trigger_error("Unknown connection type provided assumed as 'NONE'", E_USER_NOTICE);


        $this->connection = $connection;

        return $this;
    }

    /**
     * @param int $duration
     * @return $this
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @param bool $success
     * @return $this
     */
    public function setSuccess($success)
    {
        $this->success = $success;
        return $this;
    }

    /**
     * @param int|string $time
     * @return $this
     */
    public function setTime($time)
    {
        if ((string)(int)$time === (string)$time) {
            //timestamp;
            $this->createdAt = date('Y-m-d H:i:s', $time);
        } else {
            $this->createdAt = $time;
        }

        return $this;
    }

    public function setType(ActionType $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @param null $deviceID
     * @return \GuzzleHttp\Message\ResponseInterface
     * @throws UnsuccessfulCallException
     */
    public function push($deviceID = null)
    {
        if (is_null($deviceID)) {
            $deviceID = $this->_client->deviceID;
        }

        $callParams = [
            'createdAt' => strtotime($this->createdAt),
            'ID_User'   => $this->ID_User,
            'ID_Team'   => $this->ID_Team,
            'latitude'  => $this->latitude,
            'longitude' => $this->longitude,
            'online'    => $this->connection,
            'duration'  => $this->duration,
        ];


        if (!is_null($this->type)) {
            $callParams['category'] = $this->type->category;
            $callParams['type'] = $this->type->type;
            $callParams['details'] = $this->type->details;
        }

        $response = $this->_client->_post('/action', '', ['data' => json_encode([$callParams])], ['headers' => ['deviceID' => $deviceID]]);

        $responseBody = json_decode($response->getBody());

        if (!$responseBody->success) {
            throw new UnsuccessfulCallException($responseBody->errors, $this->_client->url . '/action', 'POST', 300, $response);
        }

        return $response;
    }

    /**
     * @param Client           $client
     * @param RequestInterface $requestInterface
     * @return bool|Action
     */
    public static function createFromRequest(Client $client,RequestInterface $requestInterface)
    {
        $url = explode('/', parse_url($requestInterface->getUrl(), PHP_URL_PATH));
        $url_part = array_pop($url);
        if(!array_key_exists($url_part, self::$_urlClassRelations)) {
            //assume that last part is an ID
            $url_part = array_pop($url);
        }

        if(!array_key_exists($url_part, self::$_urlClassRelations)) {
            trigger_error('Could not generate action type for action /' . $url_part . '/', E_USER_NOTICE);
            return false;
        }

        if(is_array(self::$_urlClassRelations[$url_part])) {
            if(!array_key_exists(strtoupper($requestInterface->getMethod()), self::$_urlClassRelations[$url_part])) {
                trigger_error('Could not generate action type for action /' . $url_part . '/ with method ' . $requestInterface->getMethod(), E_USER_NOTICE);
                return false;
            }

            $className = self::$_urlClassRelations[$url_part][strtoupper($requestInterface->getMethod())];
        } else {
            $className = self::$_urlClassRelations[$url_part];
        }

        if(!method_exists($className, 'createFromRequest')) {
            trigger_error('Could not generate action type for action /' . $url_part . '/ with method ' . $requestInterface->getMethod(), E_USER_NOTICE);
            return false;
        }

        $actionType = call_user_func_array([$className, 'createFromRequest'], [$requestInterface]);

        $action = new Action($client);
        $action->setUser($client->user)
            ->setTeam(1)
            ->setTime(time())
            ->setType($actionType);

        return $action;
    }
}