<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 16/12/15
 * Time: 16:52
 */

namespace Salesboard\Client\entities;


use Salesboard\Client\exceptions\LeadDuplicationException;
use Salesboard\Client\exceptions\UnsuccessfulCallException;
use Salesboard\Client\interfaces\iEntity;
use Salesboard\Client\Client;
use Salesboard\Client\nameSpaces\DataLayersNameSpace;
use Salesboard\Client\nameSpaces\FieldsNameSpace;


/**
 * @property int    ID_Lead
 * @property string hash
 * @property int    ID_User
 * @property int    ID_Team
 * @property string lastChanged
 * @property string assign_to_user
 * @property string assign_to_team
 *
 */
class Lead extends Entity implements iEntity
{
    /**
     * @var LeadField[]
     */
    public $fields;
    /**
     * @var LeadDataLayer[]
     */
    public $dataLayers;

    /**
     * @var FieldsNameSpace
     */
    private $_fieldsNameSpace = null;

    /**
     * @var DataLayersNameSpace
     */
    private $_dlsNameSpace = null;

    public function __construct(Client $client, array $params)
    {
        parent::__construct($client, $params);

        array_map([$this, '_mapFields'], (array)$this->leadFields, array_keys((array)$this->leadFields));
        array_map([$this, '_mapDLs'], (array)$this->leadDataLayers, array_keys((array)$this->leadDataLayers));

        unset($this->_fields['leadFields']);
        unset($this->_fields['leadDataLayers']);
    }

    /**
     * @inheritdoc
     */
    public static function getByID(Client $client, $id)
    {
        $response = $client->_get('/leads', ['ID_Lead' => $id]);

        $responseBody = json_decode($response->getBody());

        if (!$responseBody->success) {

            throw new UnsuccessfulCallException(
                $responseBody->errors,
                $client->url . '/leads?ID_Lead=' . $id,
                'GET',
                300,
                $response
            );
        }

        if (!empty($responseBody->warnings) && 'ERR_NO_LEADS_FOUND' === array_pop($responseBody->warnings)) {
            //nothing is found

            return null;
        }

        $responseBody = json_decode(json_encode($responseBody->responseData), true);
        return new Lead($client, array_pop($responseBody));
    }

    /**
     * @return int
     * @throws LeadDuplicationException
     * @throws UnsuccessfulCallException
     */
    public function create()
    {

        $params = [
            'leadFields'     => $this->_prepareFieldsToPost(),
            'leadDataLayers' => $this->_prepareDLsToPost(),
        ];

        if (!is_null($this->assign_to_user)) {
            $params['assign_to_user'] = $this->assign_to_user;
        }
        if (!is_null($this->assign_to_team)) {
            $params['assign_to_team'] = $this->assign_to_team;
        }

        $response = $this->_client->_post(
            '/leads',
            json_encode($params),
            [],
            ['headers' => ['Content-Type' => 'application/json']]
        );

        $responseBody = json_decode($response->getBody());

        if (!$responseBody->success) {

            if ('ERR_LEAD_DUPLICATION' === $responseBody->errors[0]) {
                throw new LeadDuplicationException($this, $response);
            }

            throw new UnsuccessfulCallException(
                $responseBody->errors,
                $this->_client->url . '/leads',
                'POST',
                300,
                $response
            );
        }

        $this->_fields['ID_Lead'] = $responseBody->responseData; //selt Lead_ID from response

        return $responseBody->responseData;
    }

    /**
     * @throws LeadDuplicationException
     * @throws UnsuccessfulCallException
     */
    public function update()
    {
        $params = [
            'ID_Lead'        => $this->ID_Lead,
            'leadFields'     => $this->_prepareFieldsToPost(),
            'leadDataLayers' => $this->_prepareDLsToPost(),
        ];

        if (!is_null($this->assign_to_user)) {
            $params['assign_to_user'] = $this->assign_to_user;
        }
        if (!is_null($this->assign_to_team)) {
            $params['assign_to_team'] = $this->assign_to_team;
        }

        $response = $this->_client->_put('/leads' . '?' . http_build_query($params), ''
        );

        $responseBody = json_decode($response->getBody());

        if (!$responseBody->success) {

            if ('ERR_LEAD_DUPLICATION' === $responseBody->errors[0]) {
                throw new LeadDuplicationException($this, $response);
            }

            throw new UnsuccessfulCallException(
                $responseBody->errors,
                $this->_client->url . '/leads',
                'PUT',
                300,
                $response
            );
        }
    }

    /**
     * @throws UnsuccessfulCallException
     */
    public function delete()
    {
        $response = $this->_client->_delete('/leads?ID_Lead=' . $this->ID_Lead);

        $responseBody = json_decode($response->getBody());

        if (!$responseBody->success) {

            throw new UnsuccessfulCallException(
                $responseBody->errors,
                $this->_client->url . '/leads',
                'DELETE',
                300,
                $response
            );
        }
    }

    /**
     * @return FieldsNameSpace
     */
    public function fields()
    {
        if (is_null($this->_fieldsNameSpace)) {
            $this->_fieldsNameSpace = new FieldsNameSpace($this->_client, $this);
        }

        return $this->_fieldsNameSpace;
    }

    /**
     * @return DataLayersNameSpace
     */
    public function dataLayers()
    {
        if (is_null($this->_dlsNameSpace)) {
            $this->_dlsNameSpace = new DataLayersNameSpace($this->_client, $this);
        }
        return $this->_dlsNameSpace;
    }

    /**
     * @param string $name
     * @return string
     */
    public function getField($name)
    {
        return $this->fields()->{$name}->value;
    }

    /**
     * @param User|string|int $user
     * @return $this
     */
    public function assignToUser($user)
    {
        if ($user instanceof User) {
            $this->_fields['assign_to_user'] = $user->username;
        } elseif ((string)(int)$user === (string)$user) {
            //integer, assign by ID
            $this->_fields['assign_to_user'] = $this->_client->getUser($user)->username;
        } else {
            //assign by username
            $this->_fields['assign_to_user'] = $user;
        }
        return $this;
    }

    /**
     * @param Team|string|int $team
     * @return $this
     */
    public function assignToTeam($team)
    {
        if ($team instanceof Team) {
            $this->_fields['assign_to_team'] = $team->name;
        } elseif ((string)(int)$team === (string)$team) {
            //integer, assign by ID
            $this->_fields['assign_to_team'] = $this->_client->getTeam($team)->name;
        } else {
            //assign by team name
            $this->_fields['assign_to_team'] = $team;
        }

        return $this;
    }

    /**
     * @return array
     */
    private function _prepareFieldsToPost()
    {
        $result = [];
        foreach ($this->fields()->all() as $field) {
            $result[$field->name] = $field->value;
        }

        return $result;
    }

    /**
     * @return array
     */
    private function _prepareDLsToPost()
    {
        $result = [];
        foreach ($this->dataLayers()->all() as $dl) {
            $result[$dl->name] = (int)$dl->enabled;
        }

        return $result;
    }

    /**
     * @param $value
     * @param $key
     */
    private function _mapFields($value, $key)
    {
        $el_array = json_decode(json_encode($value), true);

        if (is_array($el_array)) {
            //creating from API response
            $el_array['ID_Field'] = $key;
            $this->fields[$value['name']] = new LeadField($this->_client, $el_array);
        } else {
            //creating manually
            $this->fields[$key] = new LeadField($this->_client, [
                'name'  => $key,
                'value' => $value
            ]);
        }

    }

    /**
     * @param $value
     * @param $key
     */
    private function _mapDLs($value, $key)
    {
        $el_array = json_decode(json_encode($value), true);

        if (is_array($el_array)) {
            //creating from API response
            $el_array['ID_Field'] = $key;
            $this->dataLayers[$value['columnName']] = new LeadDataLayer($this->_client, $el_array);
        } else {
            //creating manually
            $this->dataLayers[$key] = new LeadDataLayer($this->_client, [
                'columnName' => $key,
                'value'      => $value
            ]);
        }
    }
}