<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 16/12/15
 * Time: 17:23
 */

namespace Salesboard\Client\entities;


use Salesboard\Client\Client;

/**
 * @property  string name
 * @property  bool   enabled
 * @property int     ID_Data_layer
 */
class LeadDataLayer extends Entity
{
    /**
     * LeadDataLayer constructor.
     * @param Client $client
     * @param array  $params
     */
    public function __construct(Client $client, array $params)
    {
        parent::__construct($client, $params);

        if (array_key_exists('enabled', $this->_fields)) {
            $this->_fields['enabled'] = boolval($this->_fields['enabled']);
        } else {
            $this->_fields['enabled'] = false;
        }
    }

    /**
     * @return $this
     */
    public function enable()
    {
        $this->enabled = true;
        return $this;
    }

    /**
     * @return $this
     */
    public function disable()
    {
        $this->enabled = false;
        return $this;
    }

    /**
     * @return $this
     */
    public function toggle()
    {
        $this->enabled = !$this->enabled;
        return $this;
    }
}