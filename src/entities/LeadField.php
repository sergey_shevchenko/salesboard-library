<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 16/12/15
 * Time: 17:13
 */

namespace Salesboard\Client\entities;

use Salesboard\Client\Client;

/**
 * @property string value
 * @property string lastUpdated
 * @property string name
 * @property string title
 * @property mixed  ID_Field
 */
class LeadField extends Entity
{
    /**
     * LeadField constructor.
     * @param Client $client
     * @param array  $params
     */
    public function __construct(Client $client, array $params)
    {
        parent::__construct($client, $params);

        $this->lastUpdated = date('Y-m-d H:i:s', $this->lastUpdated);
    }
}