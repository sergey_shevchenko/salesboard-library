<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 15/12/15
 * Time: 13:55
 */

namespace Salesboard\Client\entities;

use Salesboard\Client\Client;

/**
 * Class Entity
 * @package Salesboard\Client\entities
 */
abstract class Entity
{
    protected $_fields = [];
    protected $_client;

    /**
     * Entity constructor.
     * @param Client $client
     * @param array  $params
     */
    public function __construct(Client $client, array $params)
    {
        $this->_client = $client;
        foreach ($params as $key => $param) {
            $this->_fields[$key] = $param;
        }
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->_fields)) {
            return $this->_fields[$name];
        } elseif (property_exists(get_called_class(), $name)) {
            return $this->{$name};
        } else {
            return null;
        }
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->_fields[$name] = $value;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->_fields;
    }
}