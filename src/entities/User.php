<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 16/12/15
 * Time: 09:48
 */

namespace Salesboard\Client\entities;


use Salesboard\Client\Client;
use Salesboard\Client\exceptions\UnsuccessfulCallException;
use Salesboard\Client\interfaces\iEntity;

/**
 * @property int    ID
 * @property string email
 * @property string name
 * @property string surname
 * @property string username
 * @property string created_at
 * @property string last_sign_in_at
 * @property string role
 * @property bool   is_exported
 * @property bool   is_active
 * @property string avatar
 * @property string comments
 * @property string uppated_at
 * @property int    ID_User
 * @property array  availableScopes
 * @property array  teams
 */
class User extends Entity implements iEntity
{
    const AVATAR_SMALL = 'small';
    const AVATAR_MEDIUM = 'medium';
    const AVATAR_LARGE = 'large';

    /**
     * @param Client $client
     * @param        $id
     * @return null|User
     * @throws UnsuccessfulCallException
     */
    public static function getByID(Client $client, $id)
    {
        $response = $client->_get('/user/' . $id);

        $responseBody = json_decode($response->getBody(), true);

        if (!$responseBody['success']) {

            $error = array_pop($responseBody['errors']);

            if (404 === $error['status']) {
                //user not found
                return null;
            }

            throw new UnsuccessfulCallException(
                $responseBody->errors,
                $client->url . '/user/' . $id,
                'GET',
                300,
                $response
            );
        }

        return new User($client, $responseBody['responseData']);
    }

    /**
     * @param string $target_path
     * @param string $size
     * @return bool
     * @throws \Salesboard\Client\exceptions\APIException
     */
    public function downloadAvatar($target_path, $size = self::AVATAR_LARGE)
    {
        $response = $this->_client->_get("/avatar/$size/" . $this->avatar);

        file_put_contents($target_path, $response->getBody());

        return file_exists($target_path);
    }
}