<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 15/12/15
 * Time: 14:04
 */

namespace Salesboard\Client\entities;


use Salesboard\Client\Client;
use Salesboard\Client\exceptions\UnsuccessfulCallException;
use Salesboard\Client\interfaces\iEntity;

/**
 * Class Document
 * @property string PDF
 * @property int    ID_Document
 * @property int    ID_Form
 * @property int    ID_User
 * @property string created
 * @property string customID
 * @property array  systemVariables
 * @property string nonce
 * @property int    transfer
 * @property string syncedAt
 * @property int    retry
 * @property bool   confirmed
 * @property bool   deleted
 * @property array  fields
 * @property array  checkSums
 * @package Salesboard\Client\entities
 */
class Document extends Entity implements iEntity
{
    /**
     * @param Client $client
     * @param int    $id
     * @return Document|null
     * @throws UnsuccessfulCallException
     * @throws \Salesboard\Client\exceptions\APIException
     */
    public static function getByID(Client $client, $id)
    {
        $response = $client->_get('/documents', ['ID_Document' => $id]);

        $responseBody = json_decode($response->getBody());

        if (!$responseBody->success) {

            $error = array_pop($responseBody->errors);

            if (404 === $error->status) {
                //document not found
                return null;
            }

            throw new UnsuccessfulCallException(
                $responseBody->errors,
                $client->url . '/documents?ID_Document=' . $id,
                'GET',
                300,
                $response
            );
        }

        return new Document($client, json_decode(json_encode($responseBody->responseData), true));
    }

    /**
     * @param        $target_path
     * @return bool
     * @throws \Salesboard\Client\exceptions\APIException
     */
    public function downloadFile($target_path)
    {
        if (!$this->hasPDF()) {
            return false;
        }

        $response = $this->_client->_get('/documents/getPDF/' . $this->PDF);

        file_put_contents($target_path, $response->getBody());

        return file_exists($target_path);
    }

    /**
     * @return bool
     */
    public function hasPDF()
    {
        return !is_null($this->PDF);
    }

    public function delete()
    {
        $response = $this->_client->_delete('/documents/' . $this->ID_Document);

        $responseBody = json_decode($response->getBody());

        if (!$responseBody->success) {
            throw new UnsuccessfulCallException(
                $responseBody->errors,
                $this->_client->url . '/documents/' . $this->ID_Document,
                'delete',
                300,
                $response
            );
        }

        return true;
    }

    /**
     * @return bool
     */
    public function validateFields()
    {
        return sprintf('%u', crc32(json_encode($this->fields))) === sprintf('%u', $this->checkSums['fieldsChecksum']);
    }

    /**
     * @param string $filename
     * @return bool
     */
    public function validatePDF($filename)
    {
        return sprintf('%u', crc32(file_get_contents($filename))) === sprintf('%u', $this->checkSums['pdfCheckSum']);
    }
}