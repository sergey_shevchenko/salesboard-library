<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 16/12/15
 * Time: 11:25
 */

namespace Salesboard\Client\entities;


use Salesboard\Client\collections\TeamsCollection;
use Salesboard\Client\exceptions\APINotImplementedException;
use Salesboard\Client\interfaces\iEntity;
use Salesboard\Client\Client;

/**
 * @property int    ID_Team
 * @property int    parent
 * @property string name
 * @property string description
 * @property bool   deleted
 * @property Team[] children
 * @property bool   checked
 */
class Team extends Entity implements iEntity
{
    /**
     * Team constructor.
     * @param Client $client
     * @param array  $params
     */
    public function __construct(Client $client, array $params)
    {
        parent::__construct($client, $params);

        if (!is_null($this->children)) {
            $this->children = array_map([$this, '_mapChildren'], $this->children);
        }
    }

    /**
     * @param Client $client
     * @param int    $id
     * @return null|Team
     */
    public static function getByID(Client $client, $id)
    {
        return TeamsCollection::getInstance($client)->getByID($id);
    }

    /**
     * @param $element
     * @return Team
     */
    private function _mapChildren($element)
    {
        return new Team($this->_client, $element);
    }
}