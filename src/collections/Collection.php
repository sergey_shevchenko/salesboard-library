<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 15/12/15
 * Time: 13:15
 */

namespace Salesboard\Client\collections;

use \Salesboard\Client\Client;

/**
 * Class Collection
 * @package Salesboard\Client\collections
 */
abstract class Collection implements \Iterator
{
    /**
     * @throws \Salesboard\Client\exceptions\UnsuccessfulCallException
     * @throws \Salesboard\Client\exceptions\APIException
     */
    abstract protected function _getNextPage();


    protected $_currentResult = [];
    protected $_currentPage = 1;
    protected $_currentElement;
    protected $_currentKey;
    protected $_totalPages;
    /**
     * @var Client $_client
     */
    protected $_client;

    /**
     * Collection constructor.
     * @param Client $client
     * @param int    $page
     */
    public function __construct(Client $client, $page = 1)
    {
        $this->_client = $client;
        $this->_currentPage = 1;

        $this->__getNextPage();
    }

    /**
     * @inheritdoc
     */
    public function next()
    {
        if (empty($this->_currentResult)) {
            if ($this->_currentPage == $this->_totalPages) {
                $this->_currentElement = null;
                $this->_currentKey = null;
                return true;
            }
            $this->_currentPage++;
            $this->__getNextPage();
            if (empty($this->_currentResult)) {
                $this->_currentElement = null;
                $this->_currentKey = null;
                return true;
            }
        }
        reset($this->_currentResult);
        $this->_currentKey = key($this->_currentResult);
        $this->_currentElement = array_shift($this->_currentResult);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rewind()
    {
        $this->_currentPage = 1;
        $this->__getNextPage();
    }

    /**
     * @inheritdoc
     */
    public function valid()
    {
        return !is_null($this->_currentElement);
    }

    /**
     * @inheritdoc
     */
    public function current()
    {
        return $this->_currentElement;
    }

    /**
     * @inheritdoc
     */
    public function key()
    {
        return $this->_currentKey;
    }

    /**
     * @return int
     */
    public function getCurrentPageNum()
    {
        return $this->_currentPage;
    }

    /**
     * @param int $page
     * @return array
     */
    public function getPage($page)
    {

        if ($this->_currentPage == $page) {
            return $this->_currentResult;
        }

        $this->_currentPage = $page;
        $this->__getNextPage();
        return $this->_currentResult;
    }

    private function __getNextPage()
    {
        $this->_getNextPage();
        $this->next();
    }

}