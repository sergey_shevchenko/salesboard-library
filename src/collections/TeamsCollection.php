<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 16/12/15
 * Time: 11:32
 */

namespace Salesboard\Client\collections;


use Salesboard\Client\entities\Team;
use Salesboard\Client\exceptions\UnsuccessfulCallException;

class TeamsCollection extends StaticCollection
{
    /**
     * @var Team[]
     */
    private $_flat = [];
    /**
     * @var string[]
     */
    private $_ids = [];

    /**
     * @inheritdoc
     */
    protected function _getList()
    {
        $response = $this->_client->_get('/teams');
        $responseBody = json_decode($response->getBody());

        if (!$responseBody->success) {
            throw new UnsuccessfulCallException($responseBody->errors, $this->_client->url . '/teams', 'GET', 300, $response);
        }

        array_map(function ($element) {
            $team = new Team(
                $this->_client,
                json_decode(
                    json_encode($element)
                    , true
                )
            );
            $this->_flat[$element->name] = $team;
            $this->_ids[$element->ID_Team] = $element->name;
            $this->_currentResult[$element->ID_Team] = $team;
        }, (array)$responseBody->responseData);
    }

    /**
     * @param $id
     * @return null|Team
     */
    public function getByID($id)
    {
        if (array_key_exists($id, $this->_ids)) {
            return $this->_flat[$this->_ids[$id]];
        }

        return null;
    }
}