<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 21/12/15
 * Time: 10:32
 */

namespace Salesboard\Client\collections;


use Salesboard\Client\entities\LeadField;
use Salesboard\Client\exceptions\UnsuccessfulCallException;

class LeadFieldsStaticCollection extends StaticCollection
{
    /**
     * @inheritdoc
     */
    protected function _getList()
    {
        $response = $this->_client->_get('/leads-content/lead-fields');
        $responseBody = json_decode($response->getBody());

        if (!$responseBody->success) {
            throw new UnsuccessfulCallException($responseBody->errors, $this->_client->url . '/leads-content/lead-fields', 'GET', 300, $response);
        }

        array_map(function ($element) {
            $this->_currentResult[$element->name] = new LeadField(
                $this->_client,
                json_decode(
                    json_encode($element)
                    , true
                )
            );
        }, $responseBody->responseData->fields);

    }
}