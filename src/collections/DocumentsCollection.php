<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 15/12/15
 * Time: 14:20
 */

namespace Salesboard\Client\collections;


use Salesboard\Client\entities\Document;
use Salesboard\Client\exceptions\UnsuccessfulCallException;

/**
 * Class Documents
 * @package Salesboard\Client\collections
 */
class DocumentsCollection extends Collection
{
    /**
     * @inheritdoc
     */
    protected function _getNextPage()
    {
        $response = $this->_client->_get('/documents', ['page' => $this->_currentPage]);
        $responseBody = json_decode($response->getBody());

        if (!$responseBody->success) {
            throw new UnsuccessfulCallException($responseBody->errors, $this->_client->url . '/documents', 'GET', 300, $response);
        }

        array_map(function ($element) {
            $this->_currentResult[$element->ID_Document] = new Document(
                $this->_client,
                json_decode(
                    json_encode($element)
                    , true
                )
            );
        }, $responseBody->responseData->documents);

        $this->_totalPages = $responseBody->responseData->pagination->totalPages;
    }
}