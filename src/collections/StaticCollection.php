<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 21/12/15
 * Time: 10:19
 */

namespace Salesboard\Client\collections;

use \Salesboard\Client\Client;

abstract class StaticCollection
{
    /**
     * @throws \Salesboard\Client\exceptions\UnsuccessfulCallException
     * @throws \Salesboard\Client\exceptions\APIException
     */
    abstract protected function _getList();

    /**
     * @var $this []
     */
    private static $instances = [];

    protected $_currentResult = [];
    /**
     * @var Client $_client
     */
    protected $_client;


    /**
     * @param Client $client
     * @return $this
     */
    public static function getInstance(Client $client)
    {
        $className = get_called_class();
        if (!array_key_exists($client->url, self::$instances)) {
            self::$instances[$client->url][$className] = new $className($client);
        } elseif (!array_key_exists($className, self::$instances[$client->url])) {
            self::$instances[$client->url][$className] = new $className($client);
        }
        return self::$instances[$client->url][$className];
    }

    /**
     * Collection constructor.
     * @param Client $client
     */
    private function __construct(Client $client)
    {
        $this->_client = $client;

        $this->_getList();
    }

    /**
     * @return array
     */
    public function all()
    {
        return $this->_currentResult;
    }
}