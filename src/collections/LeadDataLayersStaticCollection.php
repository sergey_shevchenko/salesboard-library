<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 21/12/15
 * Time: 10:36
 */

namespace Salesboard\Client\collections;


use Salesboard\Client\entities\LeadDataLayer;
use Salesboard\Client\exceptions\UnsuccessfulCallException;

class LeadDataLayersStaticCollection extends StaticCollection
{
    /**
     * @inheritdoc
     */
    protected function _getList()
    {
        $response = $this->_client->_get('/leads-content/data-layers');
        $responseBody = json_decode($response->getBody());

        if (!$responseBody->success) {
            throw new UnsuccessfulCallException($responseBody->errors, $this->_client->url . '/leads-content/data-layers', 'GET', 300, $response);
        }

        array_map(function ($element) {
            $this->_currentResult[$element->name] = new LeadDataLayer(
                $this->_client,
                json_decode(
                    json_encode($element)
                    , true
                )
            );
        }, $responseBody->responseData->dataLayers);

    }
}