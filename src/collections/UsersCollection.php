<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 16/12/15
 * Time: 09:47
 */

namespace Salesboard\Client\collections;

use Salesboard\Client\entities\User;
use Salesboard\Client\exceptions\UnsuccessfulCallException;

class UsersCollection extends Collection
{
    /**
     * @inheritdoc
     */
    protected function _getNextPage()
    {
        $response = $this->_client->_get('/users', ['page' => $this->_currentPage]);
        $responseBody = json_decode($response->getBody());

        if (!$responseBody->success) {
            throw new UnsuccessfulCallException($responseBody->errors, $this->_client->url . '/documents', 'GET', 300, $response);
        }

        array_map(function ($element) {
            $this->_currentResult[$element->ID_User] = new User(
                $this->_client,
                json_decode(
                    json_encode($element)
                    , true
                )
            );
        }, $responseBody->responseData->users);

        $this->_totalPages = $responseBody->responseData->pagination->totalPages;
    }
}