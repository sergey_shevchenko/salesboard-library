<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 16/12/15
 * Time: 17:44
 */

namespace Salesboard\Client\collections;


use Salesboard\Client\entities\Lead;
use Salesboard\Client\exceptions\UnsuccessfulCallException;

class LeadsCollection extends Collection
{
    private $_filters = null;
    private $_ignoreFields = false;
    private $_startDate, $_endDate;

    /**
     * @inheritdoc
     */
    protected function _getNextPage()
    {
        $options = [
            'page' => $this->_currentPage
        ];

        if (!is_null($this->_filters)) {
            $options = array_merge(
                $this->_filters,
                $options
            );
        }
        if ($this->_ignoreFields) {
            $options['ignoreFields'] = 1;
        }

        $response = $this->_client->_get('/leads', $options);
        $responseBody = json_decode($response->getBody());

        if (!$responseBody->success) {
            throw new UnsuccessfulCallException($responseBody->errors, $this->_client->url . '/documents', 'GET', 300, $response);
        }

        array_map(function ($element) {
            $this->_currentResult[$element->ID_User] = new Lead(
                $this->_client,
                json_decode(
                    json_encode($element)
                    , true
                )
            );
        }, (array)$responseBody->responseData);

        $this->_totalPages = 1;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function addFilter($name, $value)
    {
        if (is_null($this->_filters)) {
            $this->_filters = [];
        }

        $this->_filters[$name] = $value;

        $this->rewind();

        return $this;
    }

    /**
     * @param array $filters
     * @return $this
     */
    public function setFilters(array $filters)
    {
        $this->_filters = $filters;

        $this->rewind();


        return $this;
    }

    /**
     * @return null|array
     */
    public function getFilters()
    {
        return $this->_filters;
    }

    /**
     * @return $this
     */
    public function resetFilters()
    {
        $this->_filters = null;

        $this->rewind();

        return $this;
    }

    /**
     * @param bool $ignoreFields
     * @return $this
     */
    public function ignoreFields($ignoreFields = true)
    {
        $this->_ignoreFields = $ignoreFields;
        $this->rewind();
        return $this;
    }


    /**
     * @param null|int|string $startDate - timestamp or date string YYYY-MM-DD HH:II:SS. NULL to unset
     * @param null|int|string $endDate - timestamp or date string YYYY-MM-DD HH:II:SS. NULL to unset
     * @param null|boolean    $isTimestamp
     *                              true - time parameters are timestamps,
     *                              false - time parameters are date strings,
     *                              null - autodetect
     * @return $this
     */
    public function setPeriod($startDate = null, $endDate = null, $isTimestamp = null)
    {
        if (is_null($isTimestamp)) {
            if (!is_null($startDate) && (int)$startDate == $startDate) {
                $isTimestamp = true;
            } elseif (!is_null($startDate)) {
                $isTimestamp = false;
            }
            if (!is_null($endDate) && (int)$endDate == $endDate) {
                $isTimestamp = true;
            } elseif (!is_null($endDate)) {
                $isTimestamp = false;
            }
            if (is_null($startDate) && is_null($endDate)) {
                $isTimestamp = true;
            }

        }

        if ($isTimestamp) {
            $this->_startDate = $startDate;
            $this->_endDate = $endDate;
        } else {
            $this->_startDate = strtotime($startDate);
            $this->_endDate = strtotime($endDate);
        }
        $this->rewind();
        return $this;

    }


    /**
     * @return Lead
     */
    public function one()
    {
        return $this->_currentElement;
    }
}