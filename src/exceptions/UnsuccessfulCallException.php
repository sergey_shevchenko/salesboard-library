<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 15/12/15
 * Time: 13:27
 */

namespace Salesboard\Client\exceptions;

use GuzzleHttp\Message\ResponseInterface;

/**
 * Class UnsuccessfulCallException
 * @package Salesboard\Client\exceptions
 */
class UnsuccessfulCallException extends BaseAPIException
{
    /**
     * UnsuccessfulCallException constructor.
     * @param array             $errors
     * @param string            $url
     * @param string            $method
     * @param int               $code
     * @param ResponseInterface $response
     * @param \Exception|null   $previous
     */
    public function __construct(array $errors, $url, $method, $code = 300, ResponseInterface $response, \Exception $previous = null)
    {
        $message = 'API call to (' . $method . ')' . $url . ' responded with success: false. Errors are:'
            . var_export($errors, true);

        parent::__construct($message, $code, $response, $previous);
    }
}