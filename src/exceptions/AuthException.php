<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 15/12/15
 * Time: 12:42
 */

namespace Salesboard\Client\exceptions;

use GuzzleHttp\Message\ResponseInterface;

/**
 * Class AuthException
 * @package Salesboard\Client\exceptions
 */
class AuthException extends BaseAPIException
{
    /**
     * AuthException constructor.
     * @param int               $code
     * @param ResponseInterface $response
     * @param \Exception|null   $previous
     */
    public function __construct($code = 100, ResponseInterface $response, \Exception $previous = null)
    {
        switch ($code) {
            case 100:
                $message = 'Could not authenticate: username or password is incorrect';
                break;
            case 101:
                $message = 'Could not authenticate: this user has no access to APIs';
                break;
            case 102:
                $message = 'Could not authenticate: user is expired';
                break;
            default:
                $message = 'Could not authenticate: unknown error';
        }
        parent::__construct($message, $code, $response, $previous);
    }
}