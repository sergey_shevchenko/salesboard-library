<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 16/12/15
 * Time: 13:09
 */

namespace Salesboard\Client\exceptions;


use GuzzleHttp\Message\ResponseInterface;

class BaseAPIException extends \Exception
{
    /**
     * @var ResponseInterface
     */
    private $_response;

    /**
     * BaseAPIException constructor.
     * @param string            $message
     * @param int               $code
     * @param ResponseInterface $response
     * @param \Exception|null   $previous
     */
    public function __construct($message, $code, ResponseInterface $response, \Exception $previous = null)
    {
        $this->_response = $response;

        parent::__construct($message, $code, $previous);
    }

    /**
     * @return ResponseInterface
     */
    public function getResponse()
    {
        return $this->_response;
    }
}