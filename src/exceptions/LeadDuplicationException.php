<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 17/12/15
 * Time: 15:08
 */

namespace Salesboard\Client\exceptions;


use GuzzleHttp\Message\ResponseInterface;
use Salesboard\Client\entities\Lead;

class LeadDuplicationException extends BaseAPIException
{
    /**
     * LeadDuplicationException constructor.
     * @param Lead              $lead
     * @param ResponseInterface $response
     * @param \Exception|null   $previous
     */
    public function __construct(Lead $lead, ResponseInterface $response, \Exception $previous = null)
    {
        $message = 'Lead Duplication: ' . var_export($lead->toArray(), true);
        $code = 500;

        parent::__construct($message, $code, $response, $previous);
    }
}