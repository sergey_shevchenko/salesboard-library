<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 16/12/15
 * Time: 10:16
 */

namespace Salesboard\Client\exceptions;


use GuzzleHttp\Message\ResponseInterface;

/**
 * Class APINotImplementedException
 * @package Salesboard\Client\exceptions
 */
class APINotImplementedException extends BaseAPIException
{
    /**
     * APINotImplementedException constructor.
     * @param string            $API_name
     * @param int               $code
     * @param ResponseInterface $response
     * @param \Exception|null   $previous
     */
    public function __construct($API_name, $code = 400, ResponseInterface $response, \Exception $previous = null)
    {
        $message = "API $API_name is not yet implemented. Please, contact Salesboard team for timeline update";

        parent::__construct($message, $code, $response, $previous);
    }
}