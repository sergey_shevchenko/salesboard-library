<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 15/12/15
 * Time: 12:30
 */

namespace Salesboard\Client\exceptions;

use GuzzleHttp\Message\ResponseInterface;

/**
 * Class APIException
 * @package Salesboard\Client\exceptions
 */
class APIException extends BaseAPIException
{
    /**
     * APIException constructor.
     * @param string            $responseCode
     * @param int               $method
     * @param string            $url
     * @param int               $code
     * @param ResponseInterface $response
     * @param \Exception        $previous
     */
    public function __construct($responseCode, $method, $url, $code = 001, ResponseInterface $response, \Exception $previous = null)
    {
        $message = 'Unexpected API response: ' . $responseCode
            . 'Expected code between 200 and 299. Request was: ' . $method . ' on ' . $url;
        parent::__construct($message, $code, $response, $previous);
    }
}