<?php
/**
 * Created by PhpStorm.
 * User: sergiishevchenko
 * Date: 15/12/15
 * Time: 14:32
 */

namespace Salesboard\Client\interfaces;


use Salesboard\Client\Client;

interface iEntity
{
    /**
     * @param Client $client
     * @param int $id
     * @return static
     */
    public static function getByID(Client $client, $id);
}